extern crate naivebayes;
#[cfg(feature = "serde")]
extern crate serde;
#[cfg(feature = "serde")]
extern crate serde_json;

use naivebayes::{LinearLikelihood, NaiveBayes};
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd)]
#[cfg_attr(feature = "serde", serde(rename_all = "camelCase"))]
enum Sentiment {
    Positive,
    Negative,
}

fn main() {
    use Sentiment::*;

    let mut nb = NaiveBayes::<_, _, LinearLikelihood>::default();

    nb.train("great product".split(' '), Positive);
    nb.train("the protection level is poor".split(' '), Negative);
    nb.train("this is a great band I love them".split(' '), Positive);
    nb.train("never buy this product it is too bad".split(' '), Negative);
    nb.train("i love the shoes".split(' '), Positive);
    nb.train("good product happy with the purchase".split(' '), Positive);
    let classification = nb.classify("I love it is great".split(' '));
    println!("classification = {:?}", classification);
    #[cfg(feature = "serde")]
    println!("model = \n{}", serde_json::to_string_pretty(&nb).unwrap());
}
