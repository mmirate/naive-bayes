use std::{
    borrow::Borrow,
    collections::{HashMap, HashSet},
    fmt::Debug,
    hash::Hash,
    ops::{Deref, Div, Mul},
};

#[cfg(feature = "serde")]
extern crate serde;
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

pub type Number = f64;

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug)]
struct Attributes<A, L>(
    #[cfg_attr(
        feature = "serde",
        serde(bound(
            serialize = "HashMap<A, HashMap<L, u64>>: serde::ser::Serialize",
            deserialize = "HashMap<A, HashMap<L, u64>>: Deserialize<'de>"
        ))
    )]
    HashMap<A, HashMap<L, u64>>,
);

impl<A, L> Default for Attributes<A, L> {
    fn default() -> Self {
        Attributes(Default::default())
    }
}

impl<A: Hash + Eq, L: Hash + Eq> Attributes<A, L> {
    fn add(&mut self, attribute: A, label: L) {
        let labels = self.0.entry(attribute).or_default();
        *labels.entry(label).or_insert(0) += 1;
    }

    /// How many times was the given attribute recorded with the given label?
    /// i.e. compute p(attribute AND label)
    fn frequency_of<QA: Hash + Eq + ?Sized, QL: Hash + Eq>(
        &self,
        attribute: &QA,
        label: &QL,
    ) -> (Option<u64>, bool)
    where
        A: Borrow<QA>,
        L: Borrow<QL>,
    {
        let opt = self.0.get(attribute);
        let flag = opt.is_some();
        (opt.and_then(|labels| labels.get(label).copied()), flag)
    }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug)]
struct Labels<L> {
    #[cfg_attr(
        feature = "serde",
        serde(bound(
            serialize = "HashMap<L, u64>: serde::ser::Serialize",
            deserialize = "HashMap<L, u64>: Deserialize<'de>"
        ))
    )]
    counts: HashMap<L, u64>,
}

impl<L> Default for Labels<L> {
    fn default() -> Self {
        Labels {
            counts: Default::default(),
        }
    }
}

impl<L: Hash + Eq> Labels<L> {
    fn add(&mut self, label: L) {
        *self.counts.entry(label).or_insert(0) += 1;
    }

    fn count<QL: Hash + Eq>(&self, label: &QL) -> Option<u64>
    where
        L: Borrow<QL>,
    {
        self.counts.get(label).copied()
    }

    fn labels(&self) -> impl Iterator<Item = &L> {
        self.counts.keys()
    }

    fn total(&self) -> u64 {
        self.counts.values().sum()
    }
}

pub trait Semifield:
    Sized
    + Clone
    + Copy
    + Debug
    + PartialOrd
    + Mul<Self, Output = Self>
    + Div<Self, Output = Self>
    + From<Number>
    + From<u64>
    + std::iter::Product
{
    const MIN: Self;
    fn logsumexp(v: Vec<Self>) -> Self;
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct LogLikelihood(Number);

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct LinearLikelihood(Number);

impl Deref for LogLikelihood {
    type Target = Number;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Deref for LinearLikelihood {
    type Target = Number;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl LogLikelihood {
    /// marks a number as being a logarithmic likelihood
    const fn new(x: Number) -> Self {
        LogLikelihood(x)
    }
    fn max(self, other: Self) -> Self {
        Self::new(f64::max(self.0, other.0))
    }
}

impl From<Number> for LogLikelihood {
    /// converts a member of [0,1] into a logarithmic likelihood
    fn from(x: Number) -> Self {
        Self(x.ln())
    }
}

impl From<u64> for LogLikelihood {
    fn from(x: u64) -> Self {
        (x as f64).into()
    }
}

impl From<LogLikelihood> for Number {
    /// recovers the member of [0,1] to which a logarithmic likelihood maps
    fn from(x: LogLikelihood) -> Self {
        x.0.exp()
    }
}

impl Mul<Self> for LogLikelihood {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        #[allow(clippy::clippy::suspicious_arithmetic_impl)]
        // it's a logarithm, Clippy, ya knit wit
        Self(self.0 + rhs.0)
    }
}

impl Div<Self> for LogLikelihood {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        #[allow(clippy::clippy::suspicious_arithmetic_impl)]
        // it's a logarithm, Clippy, ya knit wit
        Self(self.0 - rhs.0)
    }
}

impl From<Number> for LinearLikelihood {
    fn from(x: Number) -> Self {
        Self(x)
    }
}

impl LinearLikelihood {
    const fn new(x: Number) -> Self {
        LinearLikelihood(x)
    }
}

impl From<u64> for LinearLikelihood {
    fn from(x: u64) -> Self {
        (x as f64).into()
    }
}

impl From<LinearLikelihood> for Number {
    fn from(x: LinearLikelihood) -> Self {
        x.0
    }
}

impl Mul<Self> for LinearLikelihood {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        Self(self.0 * rhs.0)
    }
}

impl Div<Self> for LinearLikelihood {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        Self(self.0 / rhs.0)
    }
}

impl std::iter::Product for LogLikelihood {
    fn product<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.fold(1.into(), Mul::mul)
    }
}

impl std::iter::Product for LinearLikelihood {
    fn product<I: Iterator<Item = Self>>(iter: I) -> Self {
        iter.fold(1.into(), Mul::mul)
    }
}

impl Semifield for LogLikelihood {
    const MIN: Self = Self::new(-100.0);

    fn logsumexp(p: Vec<Self>) -> Self {
        let max = p
            .iter()
            .copied()
            .fold(Self::new(std::f64::NEG_INFINITY), Self::max);

        // the name "logsumexp" is prefix notation whereas the operations below are coded in postfix notation.
        // see the identifications below

        max * p
            .into_iter()
            .map(|x| x / max)
            .map(f64::from) // exp
            .sum::<f64>() // sum
            .into() // log
    }
}

impl Semifield for LinearLikelihood {
    //const ZERO: Self = Self::new(0.0);
    const MIN: Self = Self::new(1e-9);

    fn logsumexp(p: Vec<Self>) -> Self {
        // TODO LSE is the sum operation in the log semiring; in other words, according to Wikipedia,
        // "Similar to multiplication operations in linear-scale becoming simple additions in log-scale,
        // an addition operation in linear-scale becomes the LSE in log-scale."
        // Yet, the linear-probability analogue of LSE that was present before this generalization refactor,
        // was as follows, a product operation.
        p.into_iter().product()
    }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug)]
pub struct NaiveBayes<A, L, Sf: Semifield> {
    min_probability: Sf,
    #[cfg_attr(
        feature = "serde",
        serde(bound(
            serialize = "HashMap<L, u64>: serde::ser::Serialize",
            deserialize = "HashMap<L, u64>: Deserialize<'de>"
        ))
    )]
    labels: Labels<L>,
    #[cfg_attr(
        feature = "serde",
        serde(bound(
            serialize = "HashMap<A, HashMap<L, u64>>: serde::ser::Serialize",
            deserialize = "HashMap<A, HashMap<L, u64>>: Deserialize<'de>"
        ))
    )]
    attributes: Attributes<A, L>,
}

impl<A, L, Sf> Default for NaiveBayes<A, L, Sf>
where
    Sf: Semifield,
{
    fn default() -> Self {
        NaiveBayes {
            labels: Default::default(),
            attributes: Default::default(),
            min_probability: Sf::MIN,
        }
    }
}

impl<A, L, Sf> NaiveBayes<A, L, Sf>
where
    A: Hash + Eq,
    L: Hash + Eq + Clone,
    Sf: Semifield,
{
    pub fn with_min_probability(min_probability: Sf) -> Self {
        Self {
            min_probability,
            ..Default::default()
        }
    }

    fn prior<QL>(&self, label: &QL) -> Option<Sf>
    where
        Sf: Semifield,
        QL: Hash + Eq,
        L: Borrow<QL>,
    {
        Some(self.labels.total())
            .filter(|&total| total > 0)
            .and_then(|total| self.labels.count(label).zip(Some(total)))
            .map(|(label, total)| Sf::from(label) / Sf::from(total))
    }

    /// Given an attribute and label, find p(attribute AND label) and then
    /// normalize that over how many times the label occurred.
    /// If the attribute was not in the training set, return None.
    /// If the attribute was never labeled this way, return the model's minimum probability.
    /// i.e. find P(attribute | label).
    fn calculate_attr_prob<QA, QL>(&self, attribute: &QA, label: &QL) -> Option<Sf>
    where
        Sf: Semifield,
        QA: Hash + Eq + ?Sized,
        QL: Hash + Eq,
        A: Borrow<QA>,
        L: Borrow<QL>,
    {
        let (maybe_frequency, use_minimum) = self.attributes.frequency_of(attribute, label);
        maybe_frequency
            .and_then(|frequency| {
                // Option::zip doesn't lazy-evaluate,
                // else I'd just use that without the and_then
                Some(Sf::from(frequency)).zip(self.labels.count(label).map(Sf::from))
            })
            .map(|(frequency, count)| frequency / count)
            .or_else(|| Some(self.min_probability).filter(|_| use_minimum))
    }

    /// Find P(A | label) for each given attribute A, in the order given.
    fn label_prob<'a, I, QA, QL>(&self, label: &QL, attrs: I) -> Vec<Sf>
    where
        Sf: Semifield,
        I: IntoIterator<Item = &'a QA>,
        QA: Hash + Eq + ?Sized + 'a,
        QL: Hash + Eq,
        A: Borrow<QA>,
        L: Borrow<QL>,
    {
        attrs
            .into_iter()
            .filter_map(|attr| self.calculate_attr_prob::<_, _>(attr, label))
            .collect()
    }

    /// trains the model with an iterable of tokens, associating them with a label.
    pub fn train(&mut self, data: impl IntoIterator<Item = A>, label: L) {
        data.into_iter().for_each(|attribute| {
            self.attributes.add(attribute, label.clone());
        });
        self.labels.add(label);
    }

    /// classify a bag of tokens, returning a map of labels and probabilities
    /// as keys and values, respectively. Using `LogLikelihood` may prevent underflows.
    pub fn classify<'a, I, QA>(&self, data: I) -> HashMap<&L, Sf>
    where
        Sf: Semifield,
        I: IntoIterator<Item = &'a QA>,
        QA: Hash + Eq + 'a + ?Sized,
        A: Borrow<QA>,
    {
        let data: HashSet<&QA> = data.into_iter().collect();
        let mut result: HashMap<&L, Sf> = Default::default();
        let labels: HashSet<&L> = self.labels.labels().collect();
        for label in labels {
            let p = self.label_prob::<_, _, _>(label.borrow(), data.iter().copied());
            let p_iter: Sf = Semifield::logsumexp(p);
            let _ = result.entry(label).or_insert_with(|| {
                let prior = self
                    .prior(label.borrow())
                    .expect("NaiveBayes.prior returned None on a member of self.labels");
                p_iter * prior
            });
        }

        result
    }
}

#[cfg(test)]
mod test_attributes {
    use super::*;

    #[test]
    fn attribute_add() {
        let mut model = Attributes::default();
        let (rust, naive) = ("rust".to_owned(), "naive".to_owned());
        model.add(&rust, &naive);
        assert_eq!(model.frequency_of(&rust, &naive).0.unwrap(), 1);
    }

    #[test]
    fn get_non_existing() {
        let model = Attributes::<&String, &String>::default();
        assert_eq!(
            model
                .frequency_of(&"rust".to_string(), &"naive".to_string())
                .0,
            None
        );
    }
}

#[cfg(test)]
mod test_labels {

    use super::*;

    #[test]
    fn label_add() {
        let mut labels = Labels::default();
        let rust = "rust".to_string();
        labels.add(&rust);
        assert_eq!(labels.count(&rust).unwrap(), 1);
    }

    #[test]
    fn label_get_nonexistent() {
        let labels = Labels::<&String>::default();
        assert_eq!(labels.count(&"rust".to_string()), None);
    }

    #[test]
    fn get_labels() {
        let mut labels = Labels::default();
        let rust = "rust".to_string();
        labels.add(&rust);
        assert_eq!(labels.labels().count(), 1);
        assert_eq!(**labels.labels().last().unwrap(), "rust".to_string());
    }

    #[test]
    fn get_counts() {
        let mut labels = Labels::default();
        let rust = "rust".to_string();
        labels.add(&rust);
        labels.add(&rust);
        assert_eq!(labels.labels().count(), 1);
        assert_eq!(labels.count(&rust).unwrap(), 2);
    }

    #[test]
    fn get_nonexistent_counts() {
        let labels = Labels::<&String>::default();
        assert_eq!(labels.labels().count(), 0);
        assert_eq!(labels.count(&"rust".to_string()), None);
    }

    #[test]
    fn get_nonexistent_total() {
        let labels = Labels::<&String>::default();
        assert_eq!(labels.total(), 0);
    }

    #[test]
    fn get_total() {
        let mut labels = Labels::<&String>::default();
        let (rust, naive, bayes) = ("rust".to_owned(), "naive".to_owned(), "bayes".to_owned());
        labels.add(&rust);
        labels.add(&rust);
        labels.add(&naive);
        labels.add(&bayes);
        assert_eq!(labels.total(), 4);
    }
}

#[cfg(test)]
mod test_naive_bayes {

    use super::*;
    use std::f64::consts::LN_2;

    #[test]
    fn test_prior() {
        let mut nb = NaiveBayes::<_, _, LinearLikelihood>::default();
        let data: Vec<&str> = vec!["rust", "naive", "bayes"];
        nb.train(data, "👍");
        let prior = nb.prior(&"👍");
        assert_eq!(prior, Some(1.0.into()));
    }

    #[test]
    fn test_log_prior() {
        let mut nb = NaiveBayes::<_, _, LogLikelihood>::default();
        let data: Vec<&str> = vec!["rust", "naive", "bayes"];
        nb.train(data, "👍");
        let prior = nb.prior(&"👍");
        assert_eq!(prior, Some(1.0.into()));
        assert_eq!(prior, Some(LogLikelihood(0.0)));
    }

    #[test]
    fn test_prior_nonexistent() {
        let mut nb = NaiveBayes::<_, _, LinearLikelihood>::default();
        let data: Vec<&str> = vec!["rust", "naive", "bayes"];
        nb.train(data, "👍");
        let prior = nb.prior(&"👎");
        assert_eq!(prior, None);
    }

    #[test]
    #[allow(clippy::clippy::float_cmp)]
    fn test_classification() {
        let mut nb = NaiveBayes::<_, _, LinearLikelihood>::default();

        let data: Vec<&str> = vec!["rust", "naive", "bayes"];
        nb.train(data, "👍");

        let data2: Vec<&str> = vec!["golang", "java", "javascript"];
        nb.train(data2, "👎");

        let test_data: Vec<&str> = "rust scala c++".split_ascii_whitespace().collect();

        let classes = nb.classify(&test_data);
        assert_eq!(classes.get(&"👍").unwrap(), &0.5.into());
        assert_eq!(classes.get(&"👎").unwrap(), &0.0000000005.into());
        println!("{:?}", classes);
    }

    #[test]
    #[allow(clippy::clippy::float_cmp)]
    fn test_log_classification() {
        let mut nb = NaiveBayes::<_, _, LogLikelihood>::default();

        let data: Vec<&str> = vec!["rust", "naive", "bayes"];
        nb.train(data, "👍");

        let data2: Vec<&str> = vec!["golang", "java", "javascript"];
        nb.train(data2, "👎");

        let test_data: Vec<&str> = "rust scala c++".split_ascii_whitespace().collect();

        let classes = nb.classify(&test_data);

        println!("{:?}", classes);

        assert_eq!(classes.get(&"👍").unwrap(), &LogLikelihood::new(-LN_2));

        // floating-point sanity check, see ignored clippy lint above
        assert_eq!(-100.69314718055995, nb.min_probability.0 - LN_2);

        assert_eq!(
            classes.get(&"👎").unwrap(),
            &LogLikelihood::new(nb.min_probability.0 - LN_2)
        );
    }

    #[test]
    #[allow(clippy::clippy::float_cmp)]
    fn test_min_probability() {
        let mut nb = NaiveBayes::<_, _, LogLikelihood>::with_min_probability(LogLikelihood::from(
            f64::MIN_POSITIVE,
        ));
        assert_eq!(nb.min_probability, LogLikelihood::new(-708.3964185322641));

        let data: Vec<&str> = vec!["rust", "naive", "bayes"];
        nb.train(data, "👍");

        let data2: Vec<&str> = vec!["golang", "java", "javascript"];
        nb.train(data2, "👎");

        let test_data: Vec<&str> = "rust scala c++".split_ascii_whitespace().collect();

        let classes = dbg!(nb.classify(&test_data));

        dbg!(LogLikelihood::from(f64::MIN_POSITIVE));

        assert_eq!(classes.get(&"👍").unwrap(), &LogLikelihood::new(-LN_2));
        assert_eq!(
            classes.get(&"👎").unwrap(),
            &LogLikelihood::new(nb.min_probability.0 - LN_2)
        );
    }
}
